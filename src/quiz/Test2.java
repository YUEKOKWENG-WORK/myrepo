package quiz;

abstract class Programming{
	protected int count;
	public abstract int getProgramming();
	final void print() {
	}
}
public class Test2 extends Programming{

	private int number;
	Test2 (int number){
		this.number = number;
	}
	public static void main(String[] args) {

		Programming refProgramming = new Test2(3);
		System.out.println(refProgramming.getProgramming());
	}

	@Override
	public int getProgramming() {
		
		return this.number/count;
	}

}
