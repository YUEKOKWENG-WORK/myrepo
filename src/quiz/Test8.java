package quiz;

interface test123{
	 void show();
	 //void show1More();
}

public class Test8{

	public static void main(String[] args) {

		Test8 refTest8 = new Test8();
		
		test123 ref = refTest8::showAnother;
		ref.show();
	}

	static void showMe() {
		System.out.println("testing show in Test8");
	}
	void showAnother() {
		System.out.println("testing showAnother in Test8");
	}
	void showAnotherOne() {
		System.out.println("testing showAnotherOne in Test8");
	}


}
