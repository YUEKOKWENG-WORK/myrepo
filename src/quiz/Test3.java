package quiz;

class abcd {
	void m1() {
		System.out.println("non abstract");
	}
}

interface abc {
	private static void m1() {
		System.out.println("private interface");
	}
	default void m2() {
		m1();
	}
}

public class Test3 implements abc {

	public static void main(String[] args) {

		Test3 ref = new Test3();
		ref.m2();
		
		abcd ref1 = new abcd();
		ref1.m1();
	}

}
