package quiz;

import java.util.stream.Stream;
import java.util.LinkedList;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

public class Test7 {

	public static void main(String[] args) {
	
		var ref = new LinkedList<String>();
		ref.offer("hello-1");
		ref.offer("hello-2");
		ref.offer("hello-3");
		
		ref.pop();
		ref.peek();
		
		while(ref.peek()!=null) {
			System.out.println(ref.pop());
		}
		
		
		
	}

}
