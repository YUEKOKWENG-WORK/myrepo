package quiz;

public class Test9{

	public static void main(String[] args) {

		Test8 refTest8 = new Test8();
		test123 ref1 = Test8::showMe;
		ref1.show();

		test123 ref2 = refTest8::showAnother;
		ref2.show();
		
		test123 ref3 = refTest8::showAnotherOne;
		ref3.show();
	}

}
