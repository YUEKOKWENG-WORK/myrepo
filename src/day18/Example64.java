package day18;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Example64 {

	public static void main(String[] args) {
		
		ExecutorService refExecuterService = null;
		
		Runnable refRunnableTask1 = ()->
			System.out.println("Task1 - Printing Inventory..");

		Runnable refRunnableTask2 = ()->{
			for(int i=0;i<3;i++)
				System.out.println("Task2 - Printing Record.."+i);
		};

		Runnable refRunnableTask3 = ()->
		System.out.println("Task3 - Printing Inventory..");

		Runnable refRunnableTask4 = ()->{
		for(int i=0;i<3;i++)
			System.out.println("Task4 - Printing Record.."+i);
		};
		
		Runnable refRunnableTask5 = ()->{
			for(int i=0;i<3;i++)
				System.out.println("Task5 - Printing Record.."+i);
			};

		// suitable for applications that launch many short-lived tasks
		refExecuterService = Executors.newCachedThreadPool();
		
		System.out.println("Start");
		refExecuterService.execute(refRunnableTask1);
		refExecuterService.execute(refRunnableTask2);
		refExecuterService.execute(refRunnableTask1);
		refExecuterService.execute(refRunnableTask3);
		refExecuterService.execute(refRunnableTask4);
		refExecuterService.execute(refRunnableTask4);
		refExecuterService.execute(refRunnableTask4);
		refExecuterService.execute(refRunnableTask4);
		refExecuterService.execute(refRunnableTask4);
		refExecuterService.execute(refRunnableTask4);
		refExecuterService.execute(refRunnableTask5);
		
		System.out.println("End");

		
		

	}

}
