package day18;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//thread pool size is 1, core pool size is 5, max pool size is 10 and the queue is 100. 

//newFixedThreadPool(10) creates a pool with a size of 10, max size of 10


public class Example63 {

	public static void main(String[] args) {
		
		ExecutorService refExecuterService = null;
		
		Runnable refRunnableTask1 = ()->
			System.out.println("Task1 - Printing Inventory..");

		Runnable refRunnableTask2 = ()->{
			for(int i=0;i<3;i++)
				System.out.println("Task2 - Printing Record.."+i);
		};

		Runnable refRunnableTask3 = ()->
		System.out.println("Task3 - Printing Inventory..");

		Runnable refRunnableTask4 = ()->{
		for(int i=0;i<3;i++)
			System.out.println("Task4 - Printing Record.."+i);
		};

		
		refExecuterService =Executors.newFixedThreadPool(3);

		 // If all n threads are busy performing the task and additional tasks are submitted, 
		 // then they will have to be in the queue until a thread is available.
		
		 	System.out.println("Start");
			refExecuterService.execute(refRunnableTask1);
			refExecuterService.execute(refRunnableTask2);
			refExecuterService.execute(refRunnableTask1);
			refExecuterService.execute(refRunnableTask3);
			refExecuterService.execute(refRunnableTask4);
			refExecuterService.execute(refRunnableTask4);
			refExecuterService.execute(refRunnableTask4);
			refExecuterService.execute(refRunnableTask4);
			refExecuterService.execute(refRunnableTask4);
			refExecuterService.execute(refRunnableTask4);
			
			System.out.println("End");
			
	}

}
