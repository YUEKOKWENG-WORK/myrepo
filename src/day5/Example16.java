package day5;

class Company{
	
	Company refCompany = null;
	
	public Company getMethod() {
		refCompany = new Company();
		return refCompany;
	} // end of getMethod
	
	public int getNumber() {
		return 10;
	}
	
} // end of Company


class Department extends Company{
	
	Department refDepartment = null;
	
	@Override
	public Department getMethod() {
		refDepartment = new Department();
		return refDepartment;
	}
	
	/*
	 * @Override public long getNumber() { return 20; }
	 */
	
} // end of Department

public class Example16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
