package day5;

abstract class KIOSK{
	
	void payBill1() {  			// concrete method which has body ( where we can write our statement or code)
		// methood with a body part of our code
	}
	
	abstract void payBill2();	// abstract method without having any body (implementation)
	
} // end of KIOSK class

class StarHub extends KIOSK{

	@Override
	void payBill2() {
		System.out.println("pay bill..");
	}
} // end of StarHub class

class Syntel extends KIOSK{

	@Override
	void payBill2() {
		System.out.println("pay bill..");
	}
} // end of Syntel class


public class Example17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
