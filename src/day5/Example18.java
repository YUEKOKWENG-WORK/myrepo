package day5;

abstract class Microsoft{
	abstract void getMicrosoftDriver();
} // end of Microsoft

abstract class Amazon{
	abstract void getAmazonDriver();
} // end of Amazon

abstract class Google{
	abstract void getGoogleDriver();
} // end of Google


class OptimumApplication{
	
	// override all the abstarct methods from Microsoft, Amazon and Google and implement here
	
	Microsoft refMicrosoft = new Microsoft() {
		@Override
		void getMicrosoftDriver() {
				System.out.println("implement here for Microsoft..");
		}
	};
	
	Amazon refAmazon = new Amazon() {
		@Override
		void getAmazonDriver() {
			System.out.println("implement here for Amazon..");
			}
	};
	
	Google refGoogle = new Google() {
		@Override
		void getGoogleDriver() {
			System.out.println("implement here for Google..");
		}
	};
	
} // end of OptimumApplication


public class Example18 {
	public static void main(String[] args) {
			// call OptimumApplication and run application
			OptimumApplication refOptimumApplication = new OptimumApplication();
			refOptimumApplication.refMicrosoft.getMicrosoftDriver();
			refOptimumApplication.refAmazon.getAmazonDriver();
			refOptimumApplication.refGoogle.getGoogleDriver();
	}
}
