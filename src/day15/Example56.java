package day15;

class ThreadDemo1 extends Thread{
	@Override
	public void run() {
		System.out.println("ThreadDemo1 ==> Hello from run method..");
	}
} // end of ThreadDemo1

class ThreadDemo2 implements Runnable{
	@Override
	public void run() {
		System.out.println("ThreadDemo2 ==> Hello from run method..");		
	}
} // end of ThreadDemo2

public class Example56 {
	public static void main(String[] args) {
		ThreadDemo1 refThreadDemo1 = new ThreadDemo1();
		Thread refThread1 = new Thread(refThreadDemo1);
		
		refThread1.start();
		
		ThreadDemo2 refThreadDemo2 = new ThreadDemo2(); 
		Thread refThread2 = new Thread(refThreadDemo2);
		refThread2.start();
		
		Runnable refRunnable = new Runnable() {
			
			@Override
			public void run() {
				System.out.println("Runnable ==> Hello from run method..");
			}
		};
		
		Thread refThread3 = new Thread(refRunnable);
		refThread3.start();
		
		Runnable refRunnableLamda =()->{
			System.out.println("Lamda ==> Hello from run method..");
		};
		
		Thread refThread4 = new Thread(refRunnableLamda);
		refThread4.start();
		
	} // end of main

} // end of Example56
