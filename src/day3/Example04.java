package day3;

// concept of static and non-static keyword

// static ==> no need to create any object, we can access static variable or method directly by providing the class name
//        ==> ClassName.variablename or ClassName.methodNmae()


// non-static ==> we need to create object of that class to access non-static variable or method


// data member == variable + method

class Customer{
	
	static int customerID = 100;
	String customerName ="James";
	
	void getDetails1() {  // non-static
		
		// can we access customerID here? static data member
		System.out.println(customerID); // no error
		
		// can we access customerName here?
		System.out.println(customerName); // no error
		
	} // end of getDetails1()
	
	static void getDetails2() { // static


		// can we access customerID here?  static data member
		System.out.println(customerID); // no error
		
		// can we access customerName here?
		// System.out.println(customerName); //  error
		
		// to avoid the error we need to create object of Customer class
		
		Customer refCustomer = new Customer(); // we have create object of Customer class
		System.out.println(refCustomer.customerName);
		
	} // end of getDetails2()
	
} // end of Customer


public class Example04 {

	public static void main(String[] args) {
		
		// call getDetails1()
		
		// Customer.getDetails1();  // we are getting error
		
		Customer refCustomer = new Customer();
		refCustomer.getDetails1(); // non-static method
		
		// call getDetails2()
		Customer.getDetails2(); // static method
		
		// or
		
		refCustomer.getDetails2(); // static method
		
	} // end of main()

} // end of Example04
