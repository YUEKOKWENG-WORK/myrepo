package day3;

public class University {
	
	private int userversityID;
	private String universityName;
	private String universityLocation;
	
	public int getUserversityID() {
		return userversityID;
	}
	public void setUserversityID(int userversityID) {
		this.userversityID = userversityID;
	}
	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	public String getUniversityLocation() {
		return universityLocation;
	}
	public void setUniversityLocation(String universityLocation) {
		this.universityLocation = universityLocation;
	}
	
}
