package day3;

// method overloading - example 2

class Mobile{
	
	private void getFeatures(boolean wifi) { // true or false
		System.out.println(wifi);
	}
	
	public String getFeatures(String brandName) {
		return brandName;
	}
	
	String getFeatures(int mobileCode, String mobileLocation) {
		return mobileCode + " "+mobileLocation;
	}
	
	void getFeatures(long mobileDiscountOfferPrice, float mobilePrice) {
		System.out.println(mobileDiscountOfferPrice + " "+mobilePrice);
	}
	
} // end of Mobile


public class Example08 {

	public static void main(String[] args) {
	
		

	}

}
