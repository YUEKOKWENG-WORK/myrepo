package day3;

class Customer1{
	
	public String size;
}

public class SwitchCaseDemo {

	public static final double MIN_PRICE = 10;
	
	public static void main(String[] args) {
	
		//MIN_PRICE = 30;
		
		Customer1 refCustomer = new Customer1();
		
		int number = 10;
		
		switch (number) {
		case 10:
			refCustomer.size = "S";
			//break;
		case 11:
			refCustomer.size = "M";
		case 12:
			refCustomer.size = "L";
			break;

		default:
			refCustomer.size = "X";
		}
		
		System.out.println(refCustomer.size);
		
	}

}
