package day20;

import java.util.ArrayList;
import java.util.List;

class human {
	 
}

class Teacher extends human {
}

class MathTeacher extends Teacher {
}

class reliefTeacher extends MathTeacher {
}

class Student extends human {
}

public class Test123 {

	public static void addTeacher(List<? super Teacher> teacherList) {
		teacherList.add(new MathTeacher());
		teacherList.add(new reliefTeacher());
		System.out.println("Teacher Added");
	}
//	public static void addHuman(List<? extends human> reliefTeacherList) {
//		reliefTeacherList.add(new Teacher());
//		System.out.println("Teacher Added");
//	}

	public static void main(String[] args) {
		List<human> humanList = new ArrayList<human>();
		List<Teacher> teacherList = new ArrayList<Teacher>();
		List<MathTeacher> mathTeacherList = new ArrayList<MathTeacher>();
		List<Student> studentList = new ArrayList<Student>();
		addTeacher(humanList);
		addTeacher(teacherList);
		System.out.println(humanList.getClass());
		System.out.println(teacherList.getClass());

	}
}
