package day20;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Example70Self {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		List refList = new ArrayList();
		
		refList.add(new Customer(105, "Customer-5"));
		refList.add(new Customer(102, "Customer-2"));
		refList.add(new Customer(101, "Customer-1"));
		refList.add(new Customer(106, "Customer-6"));
		refList.add(new Customer(103, "Customer-3"));
		Iterator refIterator = refList.listIterator();
		System.out.println("Before sorting");
		while(refIterator.hasNext()) {
			System.out.println(refIterator.next());
		}
		
		Collections.sort(refList, new Comparator<Customer>() {

			@Override
			public int compare(Customer c1, Customer c2) {
				
				//return c1.getCustomerID() - c2.getCustomerID();
				return c1.getCustomerName().compareTo(c2.getCustomerName());
			}
			
		});
		
		System.out.println("\nAfter sorting");
		Iterator refIterator2 = refList.listIterator();
		while(refIterator2.hasNext()) {
			System.out.println(refIterator2.next());
		}

		System.out.println();
		if(refList.contains(new Customer(101,"Customer-1"))) {
			System.out.println("object found..");
		}else {
			System.out.println("object not found..");
		}
		
	}

}
