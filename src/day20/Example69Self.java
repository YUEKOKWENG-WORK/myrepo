package day20;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Example69Self {
	public static void main(String[] args) {
		List refList = new ArrayList(); //up casting
		refList.add("Hello");
		refList.add(12345);
		refList.add(true);
		Person refPerson = new Person();
		refPerson.setName("Person-1");
		refPerson.setNumber(123);
		refList.add(refPerson);

		if(refList.contains(refPerson)) {
			System.out.println("Hello - object found..");
		}else {
			System.out.println("Hello - object not found..");
		}
		
		if(refList.contains(true)) {
			System.out.println("object found..");
		}else {
			System.out.println("object not found..");
		}
		
		System.out.println(refList.isEmpty());
		System.out.println("Size of Arraylist: " + refList.size());
		
		for (int i = 0; i < refList.size(); i++) {
			System.out.println("Value at " + i + ": " + refList.get(i));
		}
			
		System.out.println();
		System.out.println("Retireve all the values by using for each from List..");
		// Retireve all the values by using for each from List
		for (Object temp : refList) {
			System.out.println(temp);
		}
		
		System.out.println("\nRetrieve all the values by using Iterator Interface..");
		// Retrieve all the values by using Iterator
		Iterator refIterator = refList.listIterator();
		while(refIterator.hasNext()) {
			System.out.println(refIterator.next());
		}
		
	}

}

class Person{
	private String name;
	private int number;
	@Override
	public String toString() {
		return name + " " + number;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setNumber(int number) {
		this.number = number;
	}
}
