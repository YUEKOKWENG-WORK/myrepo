package day20;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

// A wildcard generic type is an unknown generic type represent as ?

class ReferenceBook{ 
	void add() { }
}
class Java<T> extends ReferenceBook{ }
	class ConcurrencyInPractice<T1> extends Java<T1>{ }
		class Chapter16<T1Chapter> extends ConcurrencyInPractice<T1Chapter>{ }
			class JavaMemoryModel<Task1> extends Chapter16<Task1>{ }
	class OCPJavaSE11<T2> extends Java<T2>{ }
		class Chapter5<T2Chapter> extends OCPJavaSE11<T2Chapter>{ }
			class ArraysAndCollections<Task2> extends Chapter5<Task2>{ }
		class Chapter8<T2Chapter> extends OCPJavaSE11<T2Chapter>{ } 
			class Concurrency<Task3> extends Chapter8<Task3>{ }

public class Example68 {

	public static void main(String[] args) {
		
		// Unbounded wildcard - Syntax ?
		Java<?> refJava1 = new Concurrency<Object>();		
		
		// Wildcard with an upper bound - Syntax  ? extends type 
		Java<? extends OCPJavaSE11> refJava2 = new OCPJavaSE11<Concurrency>();		// Chapter8 also works fine
	
		// compilation error
//		Java<? extends OCPJavaSE11> refJava3 = new OCPJavaSE11<JavaMemoryModel>();		// because JavaMemoryModel does not belongs to
																						// OCPJavaSE11
		// Wildcard with a lower bound
		Java<? super Concurrency> refJava4 = new Chapter8<Concurrency>();
		
		Java<? super ArraysAndCollections> refJava5 = new Chapter8<OCPJavaSE11>();
		
		// error
//		Java<? super OCPJavaSE11> refJava6 = new Chapter8<ArraysAndCollections>();
	}
}
