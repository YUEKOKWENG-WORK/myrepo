package day4;

class Employee{
	Employee(){
		getDetails();
	}
	void getDetails() {
		new Department(101,"HR");
	}
} // end of Employee

class Department{
	int departmentID;
	String departmentType;
	Department(int departmentID, String departmentType) {
		this.departmentID = departmentID;
		this.departmentType = departmentType;
		showDetails();
	}
	void showDetails() {
		System.out.println(departmentID + " "+departmentType);
	}
} // end of Department
public class Example12 {
	public static void main(String[] args) {
		new Employee();
	}
}
