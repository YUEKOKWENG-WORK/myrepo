package day4;

// concept ==> Inheritance
// Java supports multilevel inheritance for class level
// Java supports multiple inheritance through interface

class Programming{  // base class / super class / parent class
	void showProgramming() {
		System.out.println("I am in Programming class..");
	}
} // end of Programming

class Java extends Programming { // derived class / sub class / child class
 	void showJava() {
		System.out.println("I am in Java class..");
	}
} // end of Java

class Kotlin extends Java{
	void showKotlin() {
		System.out.println("I am in Kotlin class..");
	}
} // end of Kotlin

class KotlinSecurity extends Kotlin{
	void getDetails() {
		showProgramming();
		showJava();
		showKotlin();
	} // end of getDetails()
	
} // end of KotlinSecurity

class MyClass extends KotlinSecurity{
	
	void getDetails() {
		System.out.println("data from MyClass.. ");
	}
	
	void getMyClass() {
		this.getDetails();			// which gtDetails() we are going to call?? line 36 or line 26??
		super.getDetails();
	}
	
} // 

public class Example13 {
	public static void main(String[] args) {
		new MyClass().getMyClass();				// line 40
	}
}
