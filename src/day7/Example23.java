package day7;

final class UserAccessPermission{
	
}

/*
 * class SubFinalDemo extends FinalDemo2{
 * 
 * }
 */

/*
 * final interface FinalDemo1{
 * 
 * }
 */

// can we write method as a final?
/*
 * class Mobile {
 * 
 * final void application() {
 * 
 * } }
 * 
 */// can we override final method from Mobile?

class SubMobile extends Mobile{
	
	//@Override
	/*
	 * void application() {
	 * 
	 * }
	 */
}

class Laptop {
	
	/*
	 * final int number = 10; // final variable can't be modified
	 * 
	 * void getNumber(int value) { number = value; }
	 */
	
}





public class Example23 {

	public static void main(String[] args) {
		
		

	}

}
