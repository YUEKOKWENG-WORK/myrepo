package day17;

class Producer extends Thread{
	
	StringBuffer refData;		// Thread-safe, synchronized and slow
	
	Producer(){
		refData = new StringBuffer();			// allot memory
	}
	
	@Override
	public void run() {
		synchronized (refData) {				// lock the memory (reference)
			for (int i = 0; i <=3; i++) {
				try {
					refData.append(i+ " ");    // append the data
					Thread.sleep(3000);
					System.out.println("Appending Data to StringBuffer..");
					refData.notify();         // needs to notify
				} 
				catch (Exception e) { }
			} // end of for loop
		} // end of synchronized (refData)
	} // end of run()
} //  end of Producer

class Consumer extends Thread{

	Producer refProducer;
	
	public Consumer(Producer refProducer) {
		this.refProducer = refProducer;
	}
	
	@Override
	public void run() {
		synchronized (refProducer.refData) {		// we are calling refData through the reference of Producer
			try {
				System.out.println("Before wait..");
				//refProducer.refData.wait();			// wait is false
				System.out.println("I am inside Consumer..");
			} catch (Exception e) {
			  }
			System.out.println(refProducer.refData);
		} // end of synchronized (refProducer.refData)
	} // end of run()
} // end of Consumer


public class Example60 {

	public static void main(String[] args) {
		
		Producer refProducer = new Producer();
		Consumer refConsumer = new Consumer(refProducer);
		
		Thread refThread1 = new Thread(refProducer);
		Thread refThread2 = new Thread(refConsumer);
		
		refThread1.start();
		refThread2.start();
		
	}

}
