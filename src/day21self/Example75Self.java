package day21self;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class Example75Self {

	public static void main(String[] args) {

			Queue refQueue = new LinkedList();
			refQueue.offer("a-hello-1"); // offer will word as same as add() of List and Set interface
			refQueue.offer("b-hello-1");
			refQueue.offer("c-hello-1");
			
			System.out.println(refQueue);
			
			System.out.println("\nfor each loop..");
			for (Object object : refQueue) {
				System.out.println(object);
			}

			System.out.println("\nAfter using peek() method..");
			System.out.println(refQueue.peek());
			
			System.out.println("\nUsing Iterator..");
			Iterator refIterator = refQueue.iterator();
			
			while(refIterator.hasNext()) {
				System.out.println(refIterator.next());
			}
			
			System.out.println("\nAfter using poll() method..");
			System.out.println(refQueue.poll());
			
			System.out.println("\nfor each loop..");
			for (Object object : refQueue) {
				System.out.println(object);
			}

			System.out.println("\nAfter using remove() method..");
			System.out.println(refQueue.remove());
			
			System.out.println("\nfor each loop..");
			for (Object object : refQueue) {
				System.out.println(object);
			}
	}

}

//	[a-hello-1, b-hello-1, c-hello-1]
//	
//	for each loop..
//	a-hello-1
//	b-hello-1
//	c-hello-1
//	
//	After using peek() method..
//	a-hello-1
//	
//	Using Iterator..
//	a-hello-1
//	b-hello-1
//	c-hello-1
//	
//	After using poll() method..
//	a-hello-1
//	
//	for each loop..
//	b-hello-1
//	c-hello-1