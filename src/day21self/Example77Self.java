package day21self;

import java.util.HashMap;
import java.util.Map;

public class Example77Self {

	public static void main(String[] args) {

		Map<Integer, String> refMap = new HashMap<Integer, String>();
		refMap.put(444, "valu-4");
		refMap.put(441, "valu-1");
		refMap.put(443, "valu-3");
		
		System.out.println(refMap);
		
		// By using merge()
		String result = refMap.merge(565, "value-10", (k,v) -> k + v);
		System.out.println("\nMerged Value: " +result);
		
		// Updated Map
		System.out.println("\nRetrieving data by using entrySet().forEach()");
		refMap.entrySet().forEach(ref->System.out.println(ref.getKey() + " " + ref.getValue() ));

	}

}
