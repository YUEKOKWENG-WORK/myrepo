package day21self;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Person implements Comparable<Person> {
	private String name;
	private int age;

	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {

		return name + " " + age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int compareTo(Person refPerson) {

//		return name.compareTo(refPerson.name);
		return age - refPerson.age;
	}

}

public class Example74Self {

	public static void main(String[] args) {
		var ref = new ArrayList<Person>();
		ref.add(new Person("Gmit", 69));
		ref.add(new Person("Alex", 32));
		ref.add(new Person("James", 45));

		// Comparable for a single type
		// Comparing only base on the type
		Collections.sort(ref);

		System.out.println("Comparable");
		for (Object object : ref) {
			System.out.println(object);
		}

		// Comparator for object
//		Collections.sort(ref, new Comparator<Person>() {
//
//			@Override
//			public int compare(Person a, Person b) {
//
//				return a.getName().compareTo(b.getName());
//			}
//
//		});
//		System.out.println("\nComparator");
//		for (Object object : ref) {
//			System.out.println(object);
//		}

		Comparator<Person> refComparator = (a, b) -> {
			return a.getName().compareTo(b.getName());
//			return a.getAge() - b.getAge();
		};

		Collections.sort(ref, refComparator);
		System.out.println("\nComparator");
		for (Object object : ref) {
			System.out.println(object);
		}
	}
}
