package day21self;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Example73Self {

	public static void main(String[] args) {

//		Comparable s1 ="abc";
//		Comparable s2 = new String("aBc");
//		System.out.println(s1.equals(s2));

		// can only work on a single data type
		// will give in acesending order as it only takes in one parameter
		Set refTreeSet = new TreeSet();
		refTreeSet.add("d");
		refTreeSet.add("c");
		refTreeSet.add("b");
		refTreeSet.add("g");
		refTreeSet.add("o");
		refTreeSet.add("p");
		refTreeSet.add("a");
		refTreeSet.add("r");
		refTreeSet.add("n");

		System.out.println("TreeSet - Using for each loop");
		for (Object object : refTreeSet) {
			System.out.println(object);
		}

		// HashSet is in reversed order
		Set refSet = new HashSet(); // right side type check left side type, if no left side type will be an object
									// type
		refSet.add("d");
		refSet.add("c");
		refSet.add("b");
		refSet.add("g");
		refSet.add("o");
		refSet.add("p");
		refSet.add("a");
		refSet.add("r");
		refSet.add(123);

		System.out.println(refSet);
		System.out.println("\nHashSet - Using for each loop");
		for (Object object : refSet) {
			System.out.println(object.hashCode());
		}

		Set refLinkedHashSet = new LinkedHashSet();
		refLinkedHashSet.add("D");
		refLinkedHashSet.add("C");
		refLinkedHashSet.add("B");

		// System.out.println(refSet);

		System.out.println("\nLinkedHashSetUsing for each loop");
		for (Object object : refLinkedHashSet) {
			System.out.println(object);
		}

	}

}
