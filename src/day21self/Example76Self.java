package day21self;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

// Map interface

public class Example76Self {

	public static void main(String[] args) {

		// must add in the data type you are putting in
		Map <Integer, String> refMap = new LinkedHashMap <Integer, String>();
		refMap.put(501,"d-value-1");
		refMap.put(301,"a-value-1");
		refMap.put(101,"f-value-1");
		refMap.put(201,"e-value-1");
		refMap.put(401,"c-value-1");
		refMap.put(101,"z-value-1");
		
		System.out.println("Data by using entryset");
		for (Map.Entry <Integer, String> ref : refMap.entrySet()) {
			System.out.println(ref.getKey() + " " + ref.getValue());
		}
		
		System.out.println("\nRetrieving data by using KeySet");
		System.out.println(refMap.keySet());
		
		System.out.println("\nRetrieving data by using forEach");
		refMap.forEach((key,value)->System.out.println(value));

		System.out.println("\nRetrieving data by using entrySet().forEach()");
		refMap.entrySet().forEach(ref->System.out.println(ref.getKey() + " " + ref.getValue() ));
	}
}
