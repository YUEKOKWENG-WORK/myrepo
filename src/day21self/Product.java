package day21self;

public class Product implements Comparable<Product> {

	private int productID;
	private String productName;
	
	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Override
	public String toString() {
		return productID + " " + productName;
	}

	public Product(int productID, String productName) {
		this.productID = productID;
		this.productName = productName;
	}

	// once implement Comparable need to override compareTo method
	@Override
	public int compareTo(Product refProduct) {
		
		return productID - refProduct.productID; //ascending order // swap to get descending order
		//return productName.compareTo(refProduct.productName); //comparing string
	}

}
