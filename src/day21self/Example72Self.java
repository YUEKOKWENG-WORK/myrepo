package day21self;

import java.util.ArrayList;
import java.util.Collections;

// Sorting by using Comparable Interface

public class Example72Self {

	public static void main(String[] args) {

		var refList = new ArrayList();
		refList.add(new Product(505, "Product-5"));
		refList.add(new Product(503, "Product-3"));
		refList.add(new Product(501, "Product-1"));
		refList.add(new Product(502, "Product-2"));
		refList.add(new Product(504, "Product-4"));
		
		System.out.println("Before Sorting..");
		for (Object object : refList) {
			System.out.println(object);
		}
		
		// Sorting by using Comparable interface
		Collections.sort(refList);
		
		System.out.println("\nAfter Sorting..");
		for (Object object : refList) {
			System.out.println(object);
		}
	}
}
