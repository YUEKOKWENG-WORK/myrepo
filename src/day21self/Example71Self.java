package day21self;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import day20.Customer;

public class Example71Self {

	public static void main(String[] args) {
		var refList = new ArrayList();

		refList.add(new Customer(105, "Customer-5"));
		refList.add(new Customer(102, "Customer-2"));
		refList.add(new Customer(101, "Customer-1"));
		refList.add(new Customer(106, "Customer-6"));
		refList.add(new Customer(103, "Customer-3"));
		
		Iterator refIterator = refList.listIterator();
		System.out.println("Before sorting");
		
		while(refIterator.hasNext()) {
			System.out.println(refIterator.next());
		}
		
		// Lambda Expression
		//Comparator<Customer> refComparator = (c1, c2) -> c1.getCustomerID() - c2.getCustomerID();
		Comparator<Customer> refComparator = (Customer c1, Customer c2) -> c1.getCustomerName().compareTo(c2.getCustomerName());
		Collections.sort(refList, refComparator);
		
		Iterator refIterator1 = refList.listIterator();
		System.out.println("\nAfter sorting using Lambda Expression");
		
		while(refIterator1.hasNext()) {
			System.out.println(refIterator1.next());
		}
		
		// :: Operator or Method Reference Operator
		System.out.println("\nBy using :: Operator");
		//refList.sort(Comparator.comparing(Customer::getCustomerName())); // dont add the () of the method
		refList.sort(Comparator.comparing(Customer::getCustomerID));
		
		for (Object object : refList) {
			System.out.println(object);
		}
		
		System.out.println("\nBy Reverse Order: ");
		refList.sort(refComparator.reversed());
		
		for (Object object : refList) {
			System.out.println(object);
		}
	}
}
