package day13;

interface Loan{
	void homeLoanPlan();
	void carLoanPlan();
} // end of Loan

interface Insurence{
	void individualPlan();
	void corporatePlan();
} // end of Insurence

class MyApplication {
	
	enum LoanInsurence implements Loan, Insurence{
		
		PLAN_A;

		void newMethod(){
			System.out.println("Testing..");
		}
		
		@Override
		public void individualPlan() {
			System.out.println("individualPlan");
		}

		@Override
		public void corporatePlan() {
			System.out.println("corporatePlan");
			
		}

		@Override
		public void homeLoanPlan() {
			System.out.println("homePlan");
			
		}

		@Override
		public void carLoanPlan() {
			System.out.println("carPlan");
		}
		
	} // end of enum LoanInsurence
	
	
	
	
	void getEnumData() {
		LoanInsurence.PLAN_A.homeLoanPlan();
		LoanInsurence.PLAN_A.individualPlan();
	}
	
} // end of MyApplication

// Example-45
public class EnumDemo2 {

	public static void main(String[] args) {
		
		MyApplication refMyApplication = new MyApplication();
		refMyApplication.getEnumData();
		
	}

}
