package day8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// How to take input from User by user BufferedReader Class

/*
 * class BufferReaderDemo{
 * 
 * void getname() throws IOException {
 * 
 * BufferedReader refBufferedReader = new BufferedReader(new
 * InputStreamReader(System.in)); System.out.println("Enter your name : ");
 * 
 * String name1 = refBufferedReader.readLine(); System.out.println(name1);
 * 
 * System.out.println("Enter an character : ");
 * 
 * //char charData = (char)refBufferedReader.read(); char charData =
 * refBufferedReader.readLine().charAt(3); System.out.println(charData);
 * 
 * } }
 * 
 * public class Example28 {
 * 
 * public static void main(String[] args) throws IOException {
 * 
 * new BufferReaderDemo().getname();
 * 
 * 
 * }
 * 
 * }
 */