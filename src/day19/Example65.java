package day19;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Example65 {

	public static void main(String[] args) {
		
		ExecutorService refExecuterService = null;
		
		Runnable refRunnableTask1 = ()->
			System.out.println("Task1 - Printing Inventory..");

		Runnable refRunnableTask2 = ()->{
			for(int i=0;i<3;i++)
				System.out.println("Task2 - Printing Record.."+i);
		};

		Runnable refRunnableTask3 = ()->
		System.out.println("Task3 - Printing Inventory..");

		Thread red = new Thread();
		Runnable refRunnableTask4 = ()->{
		for(int i=0;i<3;i++)
			System.out.println("Task4 - Printing Record.."+i);
		};


		refExecuterService = Executors.newScheduledThreadPool(5);
		Runnable ref = () -> {
			System.out.println("Test");
		};
		refExecuterService.execute(ref);
		
//		System.out.println("Start");
//		refExecuterService.execute(refRunnableTask1);
//		refExecuterService.execute(refRunnableTask2);
//		refExecuterService.execute(refRunnableTask1);
//		refExecuterService.execute(refRunnableTask3);
//		refExecuterService.execute(refRunnableTask4);
//		refExecuterService.execute(refRunnableTask4);
//		refExecuterService.execute(refRunnableTask4);
//		refExecuterService.execute(refRunnableTask4);
//		refExecuterService.execute(refRunnableTask4);
//		refExecuterService.execute(refRunnableTask4);
//		
//		System.out.println("End");
		
	}

}
