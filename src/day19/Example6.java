package day19;

import java.util.Scanner;
import java.util.concurrent.Executors;

class ATM2 {
    int availableBalance = 0;
    boolean depositComplete = false;

    synchronized void deposit(int amount) {		// later you can change to synchronized method
        if (!depositComplete) {
            availableBalance += amount;
            System.out.println("Available Balance after deposit: " + availableBalance);
            depositComplete = true;
            notifyAll();
        }
    }

    synchronized void withdraw(int amount) throws InterruptedException {
        if (!depositComplete) {
            wait();
        }
        if (amount > availableBalance) {
            System.out.println("Not enough lah bro!");
        } else {
            availableBalance -= amount;
            System.out.println("Available Balance after withdraw: " + availableBalance);
        }
        depositComplete = false;
    }
}

public class Example6 {

    public static void main(String[] args) {

        ATM2 refATM = new ATM2();
        Scanner refScanner = new Scanner(System.in);
        var refThread = Executors.newFixedThreadPool(2);

        while(!refThread.isShutdown()){
            System.out.println("Enter deposit amount: ");
            int depositAmount = refScanner.nextInt();

            System.out.println("Enter withdraw amount: ");
            int withdrawAmount = refScanner.nextInt();

            Runnable deposit = () -> {
            	try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
                refATM.deposit(depositAmount);
            };

            Runnable withdraw = () -> {
                try {
                    refATM.withdraw(withdrawAmount);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };

            refThread.submit(deposit);
            refThread.submit(withdraw);

            System.out.println("continue: y/n");
            String userChoice = refScanner.next();

            if (userChoice.equals("n")) {
                refThread.shutdown();
            }
        }
    }
}
