package day19;

import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class ATM1 {

    int availableBalance = 0;

    void deposit(int amount) {
        availableBalance += amount;
        System.out.println("Available Balance after deposit: " + availableBalance);
    }

    void withdraw(int amount) {
        if (amount > availableBalance) {
            System.out.println("Not enough lah bro!");
        } else {
            availableBalance -= amount;
            System.out.println("Available Balance after withdraw: " + availableBalance);
        }
    }
}

public class Example5 {

    public static void main(String[] args) {

        ATM1 refATM = new ATM1();
        Scanner refScanner = new Scanner(System.in);
        var refThread = Executors.newScheduledThreadPool(5);

        while (!refThread.isShutdown()) {
            System.out.println("Enter deposit amount: ");
            int depositAmount = refScanner.nextInt();

            System.out.println("Enter withdraw amount: ");
            int withdrawAmount = refScanner.nextInt();

            Runnable deposit = () -> {
                refATM.deposit(depositAmount);
            };

            Runnable withdraw = () -> {
                refATM.withdraw(withdrawAmount);
            };

            refThread.submit(deposit);
            refThread.schedule(withdraw, 1000, TimeUnit.MILLISECONDS);

            System.out.println("continue: y/n");
            String userChoice = refScanner.next();

            if (userChoice.equals("n")) {
                refThread.shutdown();
            }
        }
    }
}
