package day10;

abstract class ProjectManagement{
	abstract void manage();
}

abstract class SixSigma extends ProjectManagement{ }

abstract class Agile extends ProjectManagement { }

class BlackBelt extends SixSigma{

	@Override
	void manage() {
		System.out.println("Black Belt Certified..");
		
	}
	
} // end of BlackBelt

class GreenBelt extends SixSigma{

	@Override
	void manage() {
		System.out.println("Green Belt Certified..");
		
	}
	
}


class Scrum extends Agile{

	@Override
	void manage() {
		System.out.println("Scrum Certified..");
		
	}
	
}


public class Example35 {

	public static void main(String[] args) {
		
	//	ProjectManagement refProjectManagement[] = new ProjectManagement[3];
		SixSigma refSixSigma[] = new SixSigma[3];
		refSixSigma[0] = new BlackBelt();
		refSixSigma[1] = new GreenBelt();
		//refSixSigma[2] = new Scrum();

		for (int i = 0; i < 3; i++) {
			
			if(refSixSigma[i] instanceof GreenBelt)			// Generics
				refSixSigma[i].manage();
		}
	}

}
