package day10;

class OuterA{
	
	void methodOfOuterA() {			// method local inner class
		
		int number = 10;
		
		class InnerB{
			
			int number = 30;
			
			void methodOfInnerB() {
				System.out.println(number);			// 10 or 30
			} //  end of methodOfInnerB()
			
		} // end of InnerB class
		
	} // end of methodOfOuterA()

} // end of OuterA class


public class Example34 {

	public static void main(String[] args) {
	
		// print ==> number

	}

}
