
package day2;

class Employee{ // class name
	
	static int employeeID=100; // static - property or attribute or variable
	String employeeAddress="CBP"; // non-static variable
	
	static void employeeDetails1() { // static method ==> verb / action (method) 
		
		System.out.println(employeeID);
	
	} // end of employeeDetails()
	
	void employeeDetails2() {
		System.out.println(employeeAddress); // can access non-static variable
		
		System.out.println(employeeID); // can access static variable
	}
} // end of Empoloyee



public class Example02 {
	
	static String data1 = "hello-1";
	
	static String data2 ="hello-2";
	int jerico = 200;
	
	public static void main(String[] args) {
		
		// what is static?
		
		System.out.println(data1);
		System.out.println(data2);
		
		Example02 refExample02 = new Example02();
		System.out.println(refExample02.data1);
		System.out.println(refExample02.jerico);
		
		Employee.employeeDetails1(); // call method employeeDetails()
		
		// how to get access employeeDetails2()?
		//Employee.employeeDetails2();  // we are getting error
		
		// solution is to create an object of the class
		
		// how to create an object and why to create object
		Employee refEmployee = new Employee();
		refEmployee.employeeDetails2(); // now we can access non-static method of Employee class
		
 
	} // end of main
} // end of Example02
