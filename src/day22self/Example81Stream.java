package day22self;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Example81Stream {

	public static void main(String[] args) {

		int[] number = { 10, 20, 30, 40, 50 };
		String [] namelist = { "Alex", "Camit", "Bryan", "James"};
//		List<String> refList = Arrays.asList("Alex", "Amit");
//		for (String string : refList) {
//			System.out.println(refList.indexOf(string));
//		} 

		System.out.printf("Source", Arrays.toString(number));

		System.out.println("Even numbers : ");

		runStringWithLimit(Arrays.stream(namelist)).forEach(result -> System.out.print(result + " \n"));;
		
		runWithoutLimit(Arrays.stream(number));
		runWithLimit(Arrays.stream(number));
	}

	private static Stream<String> runStringWithLimit(Stream <String>refStream) {
		System.out.println("running runStringWithLimit()..");
		return refStream.filter(e -> e.charAt(0) != 'A').limit(1);
//		return refStream.filter(e -> e.equalsIgnoreCase("Alex"));
	}
	
	private static void runStringWithoutLimit(IntStream refIntStream) {
		
	}

	private static void runWithoutLimit(IntStream refIntStream) {

		System.out.println("running without limit()..");

		// filter even numbers
		refIntStream.filter(i -> i % 2 == 0).forEach(System.out::println);

	} // end of runWithoutLimit

	private static void runWithLimit(IntStream refIntStream) {

		System.out.println("Running with limit 3 ");

		// filter even numbers
		refIntStream.filter(i -> i % 2 == 0).limit(3).forEach(System.out::println);

	} // end of runWithLimit
}
