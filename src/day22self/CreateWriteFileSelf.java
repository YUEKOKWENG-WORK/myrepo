package day22self;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CreateWriteFileSelf {

	public static void createWriteFile() {
		
		// step 1:
		Scanner sc = new Scanner(System.in);
		System.out.println("Write Something: ");
		String data = sc.nextLine();
		FileWriter refFileWriter = null;

		// step 2:
		try {
			refFileWriter = new FileWriter("sample.txt"); // relative path
			// step 3:
			for (int i = 0; i < data.length(); i++) {
				refFileWriter.write(data.charAt(i));
			} // end of for loop
		} catch (IOException e) {
			System.out.println("Exception handled while writing to the file..");
//			e.printStackTrace();
		} finally {
			try {
				refFileWriter.close();
			} catch (IOException e) {
				System.out.println("Exception handled while closing the file..");
//				e.printStackTrace();
			}
		}
	}
}
