package day22self;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromFileSelf {

	public static void readFromFile() {
		FileReader refFileReader = null;
		int data;
		
		try {
			refFileReader = new FileReader("demo.txt");
			
			try {
				while ((data = refFileReader.read()) != -1) {
					System.out.print((char)data);
					
				}
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}finally {
			try {
				refFileReader.close();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
	}
}
