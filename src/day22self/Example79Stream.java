package day22self;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Example79Stream {

	public static void main(String[] args) {

		// old way of writing
		List<Integer> refList = Arrays.asList(10, 20, 30, 40, 50, 60, 70);
		
		int count1 = 0;
		
		// slows down performance when writing logic inside the for loop
		for (Integer integer : refList) {
			if (integer % 2 == 0) {
				count1++;
			}// end of if 
		}// end of for
		System.out.println(count1);
		
		// by using Stream API
		System.out.println("\nBy using Java Stream API..");
		long count2 = refList.stream().filter(integer -> integer % 2 == 0).count();
		int count3 = (int) refList.stream().filter(integer -> integer % 2 == 0).count();
		System.out.println("By Long: " + count2);
		System.out.println("By int: " + count3);
		
		// toList is a factory method / factory design pattern
		// filter is calling the test method in predicate
		List <Integer> result = refList.stream().filter(integer -> integer % 2 == 0).collect(Collectors.toList());
		System.out.println("\nBy using collect..");
		System.out.println(result);
		
	}// end of main

}// end of Example79Stream
