package day22self;

import java.util.Arrays;
import java.util.List;

public class Example78Stream {

	public static void main(String[] args) {

		// old way of writing code
		List<String> refList = null;
		refList = Arrays.asList("Paris", "Tokyo", "London");
		
		for (String string : refList) {
			System.out.println(string);
		}
		
		//new way of writing code
		System.out.println("\nUsing Lambda Expression");
		refList = Arrays.asList("Paris", "Tokyo", "London");
		refList.forEach(result ->System.out.println(result));

		// using ::
		System.out.println("\nUsing Method Reference");
		refList.forEach(System.out::println);
	}

}
