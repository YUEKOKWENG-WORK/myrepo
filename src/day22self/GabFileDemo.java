package day22self;

import java.util.Scanner;
import java.io.*;

public class GabFileDemo {

	public static void main(String[] args) {
//		Example85CreateWriteFile.createWriteFile();
//		Example85ReadFromFile.readFromFile();
		Scanner refScanner = new Scanner(System.in);

		System.out.println("Option 1: Create a File");
		System.out.println("Option 2: Read a File");
		System.out.println("Option 3. Write to File");

		System.out.println("Enter option: ");
		int userOption = refScanner.nextInt();
		switch (userOption) {
		case 1:
			System.out.println("Create a file!");
			System.out.println("Enter file name to create: ");
			String filename = refScanner.next();
			filename = filename + ".txt"; // add .txt extension
			createFile(filename);
			break;

		case 2:
			System.out.println("Reading a file!");
			System.out.println("Enter file name to read: ");
			String readfile = refScanner.next();
			readfile = readfile + ".txt"; // add .txt extension
			readFile(readfile);
			break;

		case 3:
			System.out.println("Writing to file!");
			System.out.println("Enter file name to write: ");
			String writefile = refScanner.next();
			writefile = writefile + ".txt";
			writeFile(writefile);
			break;

		default:
			break;
		}

	}

	public static void createFile(String fileName) {
		File refFile = new File(fileName);
		boolean created;
		try {
			created = refFile.createNewFile();
			System.out.println("File created: " + refFile.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void readFile(String readfile) {
		try {
			File refReadFile = new File(readfile);
			Scanner readFile = new Scanner(refReadFile);
			while (readFile.hasNextLine()) {
				String data = readFile.nextLine();
				System.out.println(data);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void writeFile(String writefile) {
		// Step 1: Get user input to write some data to the file
		Scanner refScanner = new Scanner(System.in);
		System.out.println("Write something to the file: ");
		String data = refScanner.nextLine();

		// Step 2:
		FileWriter refFileWriter = null;

		try {
			refFileWriter = new FileWriter(writefile); // Relative path

			// Step 3:
			for (int i = 0; i < data.length(); i++) {
				refFileWriter.write(data.charAt(i));
			} // End of for loop

		} catch (IOException e) {
			System.out.println("Exception handled while writing to the file...");
		}
		// Step 4: Close the file
		finally {
			try {
				refFileWriter.close(); // To .close() a FileWriter, need to surround with try catch
			} catch (IOException e) {
				System.out.println("Exception handled while closing the file...");
			}
			System.out.println();
		} // End of finally (Good practice to use finally for file handling)
	}

}
