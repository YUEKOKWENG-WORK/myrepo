package day22self;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Customer{
	private int customerID;
	private String emailAddress;
	private double bankBalance;
	public int getCustomerID() {
		return customerID;
	}
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public double getBankBalance() {
		return bankBalance;
	}
	public void setBankBalance(double bankBalance) {
		this.bankBalance = bankBalance;
	}
	public Customer(int customerID, String emailAddress, double bankBalance) {
		this.customerID = customerID;
		this.emailAddress = emailAddress;
		this.bankBalance = bankBalance;
	}
	@Override
	public String toString() {
		return customerID + " " + emailAddress + " " + bankBalance;
	}
	
	
}

public class Example83 {

	public static void main(String[] args) {
		List<Customer> refCustomerList = new ArrayList<Customer>();
		refCustomerList.add(new Customer(2,"Customer2",22.2));
		refCustomerList.add(new Customer(3,"Customer3",33.33));
		refCustomerList.add(new Customer(1,"Customer1",10.2));
		refCustomerList.add(new Customer(4,"Customer4",44.444));
		refCustomerList.add(new Customer(5,"Customer5",5555.5));
		refCustomerList.add(new Customer(6,"Customer6",5555.5));
		
		List<String> refCustomerFilterList = refCustomerList
				.stream()
				.filter(e -> e.getBankBalance() > 10.0)
				.map(e -> e.toString())
				.collect(Collectors.toList());
		System.out.println(refCustomerFilterList);
		
//		System.out.println("mapRefList");
//		Map<Double,String> mapRefList = refCustomerList
//				.stream()
//				.collect(Collectors.toMap(e->e.getBankBalance(), e->e.getEmailAddress()));
//		System.out.println(mapRefList);
		
		Customer highestBB = refCustomerList
				.stream()
				.max(Comparator.comparingDouble(e -> e.getBankBalance())).get();
		System.out.println("highestBB: " + highestBB.getBankBalance());
	
	}

}
