package day22self;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileDemo {

	public static void main(String[] args) throws IOException {
		
		File refFile = new File("./src/day22self/demo.txt");
		
		// Writing to a file
		FileOutputStream refFileOutputStream = new FileOutputStream(refFile);
		DataOutputStream refDataOutputStream = new DataOutputStream(refFileOutputStream);
		refDataOutputStream.writeUTF("Writing to the file again");

		// Reading to a file
		FileInputStream refFileInputStream = new FileInputStream(refFile);
		DataInputStream refDataInputStream = new DataInputStream(refFileInputStream);
		String readingFromFile = refDataInputStream.readUTF();
		System.out.println(readingFromFile);
	}

}
