package day22self;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Example79 {

	public static void main(String[] args) {
		
		long number1 = Stream.of("Java", "Python", "R","Kotlin","Swift").count();
		System.out.println(number1); // 3
		
		long number2 = Stream.of("Java", "Python", "R","Kotlin","Swift")
                		.mapToLong(s -> 2L)
                		.reduce(0, Long::sum);
		System.out.println(number2); // 3
		
		int number3 = Stream.of("Java", "Python", "R","Kotlin","Swift")
				.mapToInt(s -> 1)
				.reduce(0, Integer::sum);
		System.out.println(number3);
				

		double sum = DoubleStream.of(1.1,1.5,2.5,5.4)
						.reduce(0, Double::sum);
		System.out.println(sum); // 10.5

		int [] numbers = {1,2,3};
		int totalNum = IntStream.of(numbers).reduce(0, Integer::sum);
		System.out.println(totalNum);
		
		long [] numbers1 = {1,2,3};
		long totalNum1 = LongStream.of(numbers1).reduce(0, Long::sum);
		System.out.println(totalNum1);


	}

}
