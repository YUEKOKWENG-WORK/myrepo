package day6;

/*@FunctionalInterface
interface One{
	void displayMethod1();
	void displayMethod2();
}
*/

abstract class OptimumCar1{
	
	final static int number = 10;
	
	void display1() {
		System.out.println("I am in display1");
	}
	abstract void display2();
} // end of Car

interface OptimumCar2{
	
	//	void display3() {			compilation error
			// method which has body
	//	}
	
	int number = 10;		// final static
	
	default void display4() {
		display5();
		display6();
	}
	
	static void display5() {
		System.out.println("I am in display5");
	}
	
	private static void display6() {
		System.out.println("I am in display6");
	}
	
	abstract void display8();
} // end of OptimumCar2

class MyClass extends OptimumCar1 implements OptimumCar2{

	void display9() {
		display4();
	}
	
	@Override
	public void display8() {
		
		System.out.println("I  am in display8");
		
	}

	@Override
	void display2() {
		
		System.out.println("I  am in display2");
		
	}
	// can we extends more than one abstract class?? yes or no   ==> no
	// can we implements more than one interface?? yes or no   ==> yes
}

public class Example22 {

	public static void main(String[] args) {
		
		MyClass refMyClass = new MyClass();
		refMyClass.display9();
		refMyClass.display8();
		refMyClass.display2();

	}

}
