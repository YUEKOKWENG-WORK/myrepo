package day6;

class Base{
	int number = 100;
	Base(int number){		// default constructor 
		System.out.println(number);		// 100 or 10
	}
	
	void display(int number1) {
		
	}
	
} // end of Base class

class Derived extends Base{
	Derived(int number) {
		super(number);
		System.out.println(number);  // 10
	}
} // end of Derived class

class SubDerived extends Derived{
	SubDerived(int number, String data) {
		super(number);
		System.out.println(number + " "+data);  // 10 data-1
	} 
} // end of SubDerived class

public class Example21 {
	public static void main(String[] args) {
		new SubDerived(10, "data-1");
	}
} // end of Example21
