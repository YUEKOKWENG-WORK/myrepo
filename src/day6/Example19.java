package day6;

abstract class Vehicle{
	abstract void withDriver();
	abstract void driverless();
} // end of Vehicle class

abstract class Car extends Vehicle{
	abstract void supportsAI();
}


abstract class MyCar extends Car{
	// no body
} //


class Car1 extends MyCar{
	@Override
	void supportsAI() {
		System.out.println("inside supportsAI..");		
	}

	@Override
	void withDriver() {
		System.out.println("inside withDriver..");
		
	}

	@Override
	void driverless() {
		System.out.println("inside driverless..");
		
	}
	
}



public class Example19 {

	public static void main(String[] args) {
		
		

	}

}
