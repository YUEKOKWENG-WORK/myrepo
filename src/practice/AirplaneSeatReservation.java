package practice;

import java.util.Arrays;
import java.util.Scanner;

public class AirplaneSeatReservation {

	public static void main(String[] args) {

		// Declare the number of seats on the airplane
		String[][] seats = new String[5][5];
		char seatLetter = 65;

		for (int i = 0; i < seats.length; i++) {
			for (int j = 0; j < seats[i].length; j++) {
				seats[i][j] = Integer.toString(i + 1) + seatLetter;
				seatLetter++;
			}
			seatLetter = 65;
		}

		System.out.println("Airplane Seats Available For Booking: ");
		for (int i = 0; i < seats.length; i++) {
			for (int j = 0; j < seats[i].length; j++) {
				System.out.print(seats[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		String continueAdding = "Y";
		String userSeatChoice = "";
		String reservedSeats = "1A 2B 1C";
		Scanner refScanner = new Scanner(System.in);
		while (continueAdding.equalsIgnoreCase("Y")) {
			System.out.print("Enter your seat number: ");
			userSeatChoice = " " + refScanner.nextLine();
			reservedSeats += userSeatChoice;
			System.out.print("Do you want to add another seat?");
			continueAdding = refScanner.nextLine();
		}
		String[] splitOutSeats = reservedSeats.split(" ");

		// Booking of seats with X
		for (int i = 0; i < seats.length; i++) {
			for (int j = 0; j < seats[i].length; j++) {
				for (int k = 0; k < splitOutSeats.length; k++) {
					if (splitOutSeats[k].equals(seats[i][j])) {
						seats[i][j] = "X";
					}
				}
			}
		}

		// Printing out updated seats
		System.out.println("\nSeats Booked in 'X':");
		for (int i = 0; i < seats.length; i++) {
			for (int j = 0; j < seats[i].length; j++) {
				System.out.print(seats[i][j] + " ");
			}
			System.out.println();
		}
		// Removing seats row less than 4
		System.out.println("\nSeat Rows Available for 4:");
		int countNumberOfX = 0;
		for (int i = 0; i < seats.length; i++) {
			for (int j = 0; j < seats[i].length; j++) {
				if (seats[i][j].equalsIgnoreCase("X")) {
					countNumberOfX++; 
				}
			}
			if (countNumberOfX < 2) {
				for (int k = 0; k < seats[i].length; k++) {
					System.out.print(seats[i][k] + " ");
				}
			}
			countNumberOfX = 0;
			System.out.println();
		}	
	}
}
