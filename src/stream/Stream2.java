package stream;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class Stream2 {

	public static void main(String[] args) {

		List<String> refList = Arrays.asList("James", "Amit" , "Jmit");
		List<Integer> refList3 = Arrays.asList(1,2,3,1,2,3);
		
		int count = refList.stream()
				.filter(e -> e.charAt(e.length()-1) == 't')
				.mapToInt(s -> 1)
				.reduce(0, Integer::sum);
		System.out.println(count);

		int count1 = refList3.stream()
				.reduce(0, Integer::sum);

		System.out.println("count1: " + count1);


		double [] refList1 = {1.0,2.0,3.0,1.0,2.0,3.0};
		
		DoubleStream stream = DoubleStream.of(refList1);
		double sumNum = stream.sum();
		System.out.println(sumNum);

		 // OptionalDouble is a container object
        // which may or may not contain a
        // doouble value.
		DoubleStream stream2 = DoubleStream.of(refList1);
		OptionalDouble avg = stream2.average();
		System.out.println("avg: " + avg.getAsDouble());
	}

}
