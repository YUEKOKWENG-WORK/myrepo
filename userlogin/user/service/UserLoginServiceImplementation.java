
  package user.service;
  
  import java.util.Scanner;
  
  import user.dao.UserLoginDAOAuthenticationImplementation; 
  import user.dao.UserLoginDAOInterface; 
  import user.pojo.User1;
  
  public class UserLoginServiceImplementation implements UserLoginServiceInterface{
  
  UserLoginDAOInterface refUserLoginDAOInterface = null; 
  User1 refUser;
  
  @Override public void callUserDAOImplementation() {
  
  refUser = new User1();
  
  Scanner sc = new Scanner(System.in);
  System.out.println("Enter user name : "); 
  String name = sc.next();
  
  refUser.setUserLoginID(name); // it will call and set the value to User POJO class file
  
  System.out.println("Enter user password : "); 
  String password = sc.next();
  
  
  refUser.setPassword(password); // it will call and set the value to User POJO class file
  
  refUserLoginDAOInterface = new UserLoginDAOAuthenticationImplementation();
  
  refUserLoginDAOInterface.userLoginAuthentication(refUser);  // if this condition return true, then call user dash board 
                                                              // else redirect to user login home page
  
  }
}
 