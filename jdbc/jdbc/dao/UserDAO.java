package jdbc.dao;

import jdbc.pojo.User;

public interface UserDAO {
	
	void getUserRecord();
	void insertRecord(User refUser);
	void deleteRecord(User refUser);
	void updateRecord(User refUser);
	void getUserByID(User refUser);
	// void userLogin();
	
}
