package jdbc.service;

import java.util.Scanner;

import jdbc.dao.UserDAO;
import jdbc.dao.UserDAOImpl;
import jdbc.pojo.User;

public class UserServiceImpl implements UserService{

	UserDAO refUserDAO;
	Scanner scannerRef;
	User refUser;
	
	public void userInputInsertRecord() {
		scannerRef = new Scanner(System.in);
		
		System.out.println("Enter User ID : ");
		int userLoginID = scannerRef.nextInt();
		
		System.out.println("Enter User Password : ");
		String userPassword = scannerRef.next();
		
		refUser = new User();
		refUser.setUserID(userLoginID);
		refUser.setUserPassword(userPassword);
		
		refUserDAO = new UserDAOImpl();
		refUserDAO.insertRecord(refUser);
	
	} // end of userInputInsertRecord
	
	
	public void userChoice() {  // router
		
		System.out.println("Enter Choice");
		scannerRef = new Scanner(System.in);
		int choice = scannerRef.nextInt();
		switch (choice) {
		case 1:
			userInputInsertRecord();
			break;

		case 2:
			getUserRecord();
			break;
		
		case 3:
			updateUserRecord();
			break;
			
		case 4:
			deleteRecord();
			break;
			
		case 5:
			getUserByID();
			break;
			
		default:
			System.out.println("Option not found..");
			break;
		} // end of userChoice
	
	} // end of userChoice




	@Override
	public void getUserRecord() {
		refUserDAO = new UserDAOImpl();
		refUserDAO.getUserRecord();
	}


	@Override
	public void updateUserRecord() {
		scannerRef = new Scanner(System.in);
		
		System.out.println("Enter User ID : ");
		int userLoginID = scannerRef.nextInt();
		
		System.out.println("Enter New User Password : ");
		String userPassword = scannerRef.next();
		
		refUser = new User();
		refUser.setUserID(userLoginID);
		refUser.setUserPassword(userPassword);
		
		refUserDAO = new UserDAOImpl();
		
		refUserDAO.updateRecord(refUser);
	}



	// there is a difference between delete and truncate
	// delete means we can retrieve back, but in case of truncate we can't retrieve back
	@Override
	public void deleteRecord() {
		
		scannerRef = new Scanner(System.in);
		
		System.out.println("Enter User ID : ");
		int userLoginID = scannerRef.nextInt();
		
		refUser = new User();
		refUser.setUserID(userLoginID);
		
		refUserDAO = new UserDAOImpl();
		refUserDAO.deleteRecord(refUser);
			
		
	}




	@Override
	public void getUserByID() {
		
		scannerRef = new Scanner(System.in);
		
		System.out.println("Enter User ID : ");
		int userLoginID = scannerRef.nextInt();
		
		refUser = new User();
		refUser.setUserID(userLoginID);
		
		refUserDAO = new UserDAOImpl();
		refUserDAO.getUserByID(refUser);
		
	}
	
} // UserServiceImpl
